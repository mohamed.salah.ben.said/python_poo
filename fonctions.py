def message():
    print("ceci est un message")


def calcul_carre(reel):
    print(reel**2)


def calcul_carre_return(reel):
    return reel**2


def calcul_produit(x, y):
    return x*y


def calcul_puissance(x, y):
    return x**y


def calculatrice(x, y, operation='unknown'):
    if operation == "produit":
        return calcul_produit(x, y)
    elif operation == "puissance":
        return calcul_puissance(x, y)
    else:
        return "l'operation n'est pas reconnue"


message()
"*****"
calcul_carre(2)
"****"
resultat = calcul_carre_return(3)
print("le resultat de la fonction est " + str(resultat))
print(calcul_produit(2, 8))

resultat2 = calculatrice(3, 5, "puissance")
print(resultat2)

# arguments nommés :
resultat3 = calculatrice(x=3, y=5)
print(resultat3)

# args


def ma_fonction_affichage(prenom, nom, **autres):
    print(prenom)
    print(nom)
    for cle, valeur in autres.items():
        print(valeur)


ma_fonction_affichage(age=30, ville="Lyon", prenom="Youssef", nom="El Behi")

# kwargs

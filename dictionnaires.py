animal = {}
print(type(animal))

# initialisation d'un dictionnaire 'animal'
# clés : nom, taille, poids
# valeurs :"Girafe",12, 1200

names = ["heribovores, ruminant"]

animal = {
    "nom": "Girafe",
    "poids": 1200,
    "carateristics": {
        "nom": names,
        "matricule": 12
    }
}

# verifier le type
if type(animal['carateristics']) is dict:
    print("carateristics est un dictionnaire")


print(animal)
print(animal["nom"])

animal["nom"] = "Lyon"
animal["region"] = "sud"
print(animal["carateristics"]["nom"])

if "poids" in animal:
    print("la clé nom existe")
    print(animal["poids"])
else:
    print("la clé n'existe pas")

result1 = animal.pop("poids", 99)
print(result1)
print(animal)

# verification de l'existance d'une clé

if "poids" in animal:
    print("la clé nom existe")
    print(animal["poids"])
else:
    print("la clé n'existe pas")


# méthode update() : mise à jour du dictionnaire

more_caracteristics = {
    "color": "yellow",
    "speed": 25,
    "voice": "unknown"
}

animal['carateristics'].update(more_caracteristics)
print(animal)


# iteration sur les dictionnaires
print(animal.items())

for key, value in animal.items():
    print(value)
    if type(value) is int:
        print("voici la valeur int " + str(value))


print(animal.keys())
print(animal.values())

# tuples

first_tuple = (1, 2, 5, 9)
first_list = [1, 2, 5, 9]
print(first_tuple)
print(first_tuple[0])
first_list[0] = 899
my_new_list = tuple(first_list)
print(my_new_list)

my_set = {1, 2, 3, 4, 4, 2}
my_list = [1, 2, 3, 4, 4, 2]
my_tuple = (1, 2, 3, 4, 4, 2)

my_second_tuple = (1, my_tuple)

print(my_set)
print(my_list)
print(my_tuple)
print(my_second_tuple)
print(my_second_tuple[1][0])
